import React, { Component } from "react";
import JugTourComponent from "./jugTourComponent";
class JugTourListComponent extends Component {
  constructor() {
    super();
    this.state = {
      tourList: localStorage.getItem("TOUR_LIST")
        ? JSON.parse(localStorage.getItem("TOUR_LIST"))
        : [],
      showTourForm: false,
      editHeading: false,
      selectedTour: {},
      tourObj: {
        name: "",
        address: "",
        city: "",
        state: "",
        country: "",
        postalCode: "",

      }
    };
  }

  onChange = e => {
    this.setState({
      tourObj: { ...this.state.tourObj, [e.target.name]: e.target.value }
    });
  };
  saveTour = tour => {
    this.cancelTour();
    this.state.tourList.push(tour);
    this.setState({ tourList: [...this.state.tourList] });
    localStorage.setItem("TOUR_LIST", JSON.stringify(this.state.tourList));
  };

  addTour = () => {
    this.setState({ showTourForm: true, editHeading: false, tourObj: {} });
  };

  cancelTour = () => {
    this.setState({ showTourForm: false });
  };

  editTour = tour => {
    this.setState({ tourObj: tour, showTourForm: true, editHeading: true });
  };

  deleteTour = index => {
    this.state.tourList.splice(index, 1);
    this.setState({ tourList: [...this.state.tourList] });
    localStorage.setItem("TOUR_LIST", JSON.stringify(this.state.tourList));
  };

  getTourList = () => {
    // console.log(tourListContent)
    let tourListContent = [];
    this.state.tourList.map((tour, index) => {
      tourListContent.push(
        <tr key={index}>
          <td>{tour.name}</td>
          <td>{tour.address}</td>
          <td>{tour.city}</td>
          <td>{tour.state}</td>
          <td>{tour.country}</td>
          <td>{tour.postalCode}</td>
          <td>
            <div className="row">
              <div className="col-12 col-md-6">
                <button
                  type="button"
                  class="btn btn-cancel btn-block btn-lg"
                  onClick={() => this.editTour(tour)}
                >
                  Edit
            </button>
              </div>
              <div className="col-12 col-md-6">
                <button
                  type="button"
                  class="btn btn-delete btn-block btn-lg"
                  onClick={() => this.deleteTour(index)}
                >
                  Delete
            </button>
              </div>
            </div>


          </td>
        </tr>
      );
    });
    return tourListContent;
  };

  render() {
    console.log(this.state.tourList)
    return (
      <>
        {!this.state.showTourForm && (
          <div className="container mt-5 border py-3">
            <div className="row mb-3 ">
              <div className="col-6 col-md-3">
                <h4>My Jug Tour</h4>

              </div>
              <div className="col-6 col-md-9 text-right">
                <button
                  type="button"
                  class="btn btn-lg btn-submit"
                  onClick={() => this.addTour()}
                >
                  Add tour
            </button>
              </div>
            </div>

            {this.state.tourList.length == 0 && (
              <div className="row">
                <div className="col-12">
                  <p className="error text-center">Your tour list is empty</p>
                </div>
              </div>
            )}

            {this.state.tourList.length > 0 &&
              <div className="table-responsive">
                <table class="table table-bordered ">
                  <thead>
                    <tr className="text-center">
                      <th scope="col">Name</th>
                      <th scope="col">Address</th>
                      <th scope="col">City</th>
                      <th scope="col">State</th>
                      <th scope="col">Country</th>
                      <th scope="col">Zip</th>
                      <th scope="col">Actions</th>
                    </tr>
                  </thead>
                  <tbody>{this.getTourList()}</tbody>
                </table>
              </div>
            }
          </div>
        )}
        {this.state.showTourForm && (
          <JugTourComponent
            // saveTour={() => this.saveTour()}
            // cancelTour={() => this.cancelTour()}
            // onChange={() => this.onChange()}
            {...this.state}
            {...this}
          />
        )}
      </>
    );
  }
}

export default JugTourListComponent;
