import React, { Component } from "react";

class JugTourComponent extends Component {
  render() {
    var {
      name,
      address,
      city,
      state,
      country,
      postalCode,
    } = this.props.tourObj;
    console.log(this.props)
    return (
      <form className="container border py-3 mt-3">
        <div className="row my-3">
          <div className="col-12">

            {this.props.editHeading ? (
              <h3>
                Edit Tour
              </h3>
            ) : (
                <h3>
                  Add Tour
                </h3>
              )}

          </div>
        </div>
        <div className="form-group">
          <label htmlFor="inputEmail4">Name</label>
          <input
            type="text"
            className="form-control"
            id="inputName"
            name="name"
            value={name}
            maxLength={50}
            onChange={e => this.props.onChange(e)}
          />
        </div>
        <div className="form-group">
          <label htmlFor="inputAddress">Address</label>
          <input
            type="text"
            className="form-control"
            id="inputAddress"
            name="address"
            value={address}
            maxLength={100}

            onChange={e => this.props.onChange(e)}
          />
        </div>
        <div className="form-group">
          <label htmlFor="inputCity">City</label>
          <input
            type="text"
            className="form-control"
            id="inputCity"
            name="city"
            value={city}
            maxLength={50}

            onChange={e => this.props.onChange(e)}
          />
        </div>
        <div className="form-row">
          <div className="form-group col-md-5">
            <label htmlFor="inputState">State</label>
            <input
              type="text"
              className="form-control"
              id="inputState"
              name="state"
              value={state}
              maxLength={50}

              onChange={e => this.props.onChange(e)}
            />
          </div>
          <div className="form-group col-md-5">
            <label htmlFor="inputCountry">Country</label>
            <input
              type="text"
              className="form-control"
              id="inputCountry"
              name="country"
              value={country}
              maxLength={50}
              onChange={e => this.props.onChange(e)}
            />
          </div>
          <div className="form-group col-md-2">
            <label htmlFor="inputZip">Postal Code</label>
            <input
              type="text"
              className="form-control"
              id="inputZip"
              name="postalCode"
              maxLength={6}

              value={postalCode}
              onChange={e => this.props.onChange(e)}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-6">
            <button
              type="submit"
              className="btn btn-block btn-lg btn-submit"
              onClick={() => this.props.saveTour(this.props.tourObj)}
            >
              Save
        </button>
          </div>
          <div className="col-6">
            <button
              type="submit"
              className="btn btn-block btn-lg btn-cancel"
              onClick={() => this.props.cancelTour()}
            >
              Cancel
        </button>
          </div>
        </div>

      </form>
    );
  }
}

export default JugTourComponent;
