import React from "react";
import { Route, Switch } from "react-router-dom";
import JugTourListComponent from "../screens/jugTour/views/jugTourListComponent";
class RoutesComponent extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Switch>
          <Route
            path="/"
            exact
            render={props => {
              return (
                <React.Fragment>
                  <JugTourListComponent {...this.props} {...props} />
                </React.Fragment>
              );
            }}
          />
        </Switch>
      </React.Fragment>
    );
  }
}

export default RoutesComponent;
